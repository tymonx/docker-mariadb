# Docker MariaDB

Proof of concept of working auto-bootstrap and auto-join for
[MariaDB](https://mariadb.com) database with [Galera Cluster](https://galeracluster.com)
replication [Docker image](https://hub.docker.com/_/mariadb) in multi containers scenario.

[[_TOC_]]

## Docker Compose

```plaintext
docker-compose --project-name db up
```

Scaling using environment variable:

```plaintext
CLUSTER_ADDRESS="gcomm://db_node_1,db_node_2,db_node_3,db_node_4,db_node_5"
docker-compose --project-name db up --scale node="$(echo "${CLUSTER_ADDRESS}" | tr ',' ' ' | wc -w)"
```

Scaling using `mysql` configuration file:

```plaintext
docker-compose --project-name db up --scale node="$(grep -i wsrep_cluster_address <name>.cnf | tr -d ' ' | tr ',' ' ' | wc -w)"
```
